Ein paar Tools, die ich oft oder immer benutze:

## CLI
[iTerm](https://www.iterm2.com/) Netter Ersatz fuer das OSX Terminal.

[Fish Shell](https://fishshell.com/) Alternative zu Bash. Macht auf alle Fälle Spass. Kannst dann natürlich bei Bedarf immer noch Bash Befehle auslösen. Es gibt noch einige andere wie z.B.  [Z Shell](http://www.zsh.org/). Schau dich um.

## Package Manager (OSX)
[Homebrew](http://brew.sh/) Ohne Package Manager wirst du früher oder später ne Menge Zeit verlieren. Die Idee eines Package Managers ist, dir die Instandhaltung deiner Umgebungen insofern zu erleichtern, dass du im Besten Fall mit einem "One Liner" alles auf den neuesten (funktionierenden oder ganz einfach nur neuesten) Stand bringen kannst. Stichwort: dependency hell.

[MacPorts](https://www.macports.org/) geht auch. 

[Caskroom](https://caskroom.github.io/) Eine Erweiterung für Brew für macOS Apps. Mag ich gerne, ist aber nicht nötig.

## Web Dev

[Codekit](https://incident57.com/codekit/) Kein bock auf command-line und das ganze task setup? Verwende dieses Tool. Sehr angenehm.

[Atom](https://atom.io/)  
Dazu würde ich unbedingt noch das Package "Emmet" installieren. Am besten übers Terminal:

```
apm install emmet
```  
apm steht für Atom Package Manager, ein CLI Tool, das mit Atom mitgeliefert wird (glaub ich zumindest).

[Emmet](http://emmet.io/) hilft dir vor allem dabei, schneller HTML zu schreiben. Du schreibst beispielsweise das:

```
ul.girls>li.girl#girl_$*3>img
```  
drückst die Tabulatortaste und erhältst das:

```
<ul class="girls">
  <li class="girl" id="girl_1"><img src="" alt=""></li>
  <li class="girl" id="girl_2"><img src="" alt=""></li>
  <li class="girl" id="girl_3"><img src="" alt=""></li>
</ul>
```

[Sass](http://sass-lang.com/)  hilft dir dabei schneller, aber auch lesbareres CSS zu schreiben.  
Du schreibst beispielsweise das:

```
$black: #222;
$red: #ff1133;

@mixin font-size($size: _) {
  @if $size == large {
    font-size: 1.6rem;
  } @else {
    font-size: 1.2rem;
  }
}

.foo {
  border: 1px solid $black;

  &.bar {
    border-color: $red;
  }

  .baz {
    @include font-size(large);

    &::first-line {
      text-indent: 2em;
    }
  }
  
  .qux {
    @include font-size();
  }
}
```

und es wird zu folgendem CSS transpiliert:

```
.foo {
  border: 1px solid #222;
}
.foo.bar {
  border-color: #ff1133;
}
.foo .baz {
  font-size: 1.6rem;
}
.foo .baz::first-line {
  text-indent: 2em;
}
.foo .qux {
  font-size: 1.2rem;
}
```


### Libs
#### Sass
coming soon.