### Yo

Unsere Repo um Files auszutauschen. Weil ich gerade dabei war, dir die Uni Folien per E-Mail zu schicken, dachte ich, es waere besser gleich mit git anzufangen. Super Tool, womit viele Leute arbeiten, besonders aus der IT-Branche. Praktisch unverzichtbar in Teams. Mach dich schlau. Ich denke, man muss darin kein Experte sein, Grundkenntnisse sollte aber jeder Typ haben, der sich mit Software jeglicher Art auseinandersetzen moechte.

Ich wuerd am Anfang die Befehle in der Konsole (REPL) eingeben und erst dann bei Bedarf auf ein Interface wie z.B. Sourcetree wechseln. Ich denke, so kriegst du eher ne Ahnung von den Begriffen, die dir schlussendlich den Umgang mit einem git Interface erleichtern.

Cheers, J
