import java.util.LinkedList;
import java.util.ListIterator;
import java.util.List;

/**
   A program that demonstrates the LinkedList class
   (source: C. Horstmann, Java essentials)
*/
public class ListTester
{
   public static void main(String[] args)
   {
      List<String> staff = new LinkedList<String>();
      staff.add("Dick");
      staff.add("Harry");
      staff.add("Romeo");
      staff.add("Tom");

      // | in the comments indicates the iterator position

      ListIterator<String> iterator
            = staff.listIterator(); // |DHRT
      iterator.next(); // D|HRT
      iterator.next(); // DH|RT

      // Add more elements after second element

      iterator.add("Juliet"); // DHJ|RT
      iterator.add("Nina"); // DHJN|RT

      iterator.next(); // DHJNR|T

      // Remove last traversed element

      iterator.remove(); // DHJN|T

      // Print all elements

      for (String name : staff)
         System.out.println(name);
   }
}
